/*
// Full scene dynamic GI system
Limitations:
	
	
Data Structures:
	Shared:
		1) Probe ID - Determines offset into probe texture array.
		3) Probe Generation Command { int index;  }
	CPU:
		1) Standard light probe organization data structure
	GPU:
		1) Probe Texture Array/Buffer - Contains directionally encoded color data.  Contained either as a spherical harmonic or some other encoding
		
Procedure:
*/
#include <vector>
#define GRAPHICS_TIMER(tg)

namespace std
{
	template <typename T>
	struct vector_view
	{
	public:
		inline vector_view(std::vector<T>& v, size_t s, size_t e)
			: vec(v), start(s), end(e < v.size() ? e : v.size()) {}

		inline size_t size() const
		{
			return end - start;
		}

		inline T& operator[](size_t index)
		{
			return vec[start + index];
		}
		
		inline const T& operator[](size_t index) const
		{
			return vec[start + index];
		}

		inline std::vector<T>& vector()
		{
			return vec;
		}

	private:
		std::vector<T>& vec;
		size_t start;
		size_t end;
	};
}

class Universe;
class MeshRenderer;

struct acceleration_structure_t
{
	void CPUUpdate();
	void ShaderUpdate();
};

struct Texture
{
	void Use();
};


struct ComputeBuffer
{
	template<typename T>
	inline void Upload(const std::vector_view<T>& src){/* TODO */}
};

struct ComputeBufferHandle
{
	// these should be allocated from a refcounted slotmap
	ComputeBuffer* operator->();
};

struct ComputeShader;

struct ComputeShaderHandle
{
	// these should be allocated from a refcounted slotmap
	ComputeShader* operator->();
};

struct TextureHandle
{
	TextureHandle copyWithNoRefcounting();

	// these should be allocated from a refcounted slotmap
	Texture* operator->();
};

struct ComputeShader
{
	void Use();
	void SetTexture(int targetTextureBinding, TextureHandle);
	void SetImage(int targetTextureBinding, TextureHandle);
	void Execute(int threadsX, int threadsY, int threadZ);
};

struct raycast_cmd_t
{
	float originX;
	float originY;
	float originZ;
	float stub;

	float dirX;
	float dirY;
	float dirZ;
	float stub2;
};

// GRAPHICS_TIMER(int64_t*) macro adds a scoped timer 
class RayTracingSystem
{
	ComputeShaderHandle computeShader;
	ComputeBufferHandle cmdBufferHandle;

	int CSTargetTextureSlot;
	int CSCmdBufferSlot;
	bool sceneDirty;

	acceleration_structure_t* rayAccelerationStructure;

public:
	inline void UpdateAccelerationStructure(Universe& uni)
	{
		rayAccelerationStructure->CPUUpdate();
	}

	inline void RaytaceIntoScene(const std::vector_view<raycast_cmd_t>& commands, TextureHandle targetTexture)
	{
		// Set up compute shader
		computeShader->Use();
		computeShader->SetImage(CSTargetTextureSlot, targetTexture);
		cmdBufferHandle->Upload(commands);
		if (sceneDirty) rayAccelerationStructure->ShaderUpdate();
		
		// TODO: Figure out better thread dimensions
		computeShader->Execute(commands.size(), 1, 1);
	}

	inline size_t GetMaxSinglePassRays()
	{
		return 1024;
	}
};

struct probe_t
{
	// TODO: Data here.  Bounding box, index in texture, etc
	void GenerateRays(std::vector<raycast_cmd_t>&, int rayCount);
	void CalculateProbe();
};

class ProbeRenderingSystem
{
	std::vector<probe_t> probes;

	TextureHandle rayResult;
	TextureHandle LightProbe;

	int64_t lastProbeDurationNanoSeconds;
	int indexFromLastFrame = 0;
	int index = 0;
	int raycountPerProbe = 192;

private:
	/* Must return a minimum of 1 */
	size_t CalculateProbeCountThisFrame();

public:
	void ComputeProbesFromRayResult(size_t index, size_t probeCount)
	{
		for(size_t i = 0; i < probeCount; ++i)
		{
			index = (indexFromLastFrame + i) % probes.size();
			
			auto& probe = probes[index];
			// probably not the best actual program flow for performance, but you get it
			probe.CalculateProbe();
		}
	}

	void RenderProbes(RayTracingSystem* rayTracer, int numToRender)
	{
		static thread_local std::vector<raycast_cmd_t> cmd;
		cmd.clear();

		GRAPHICS_TIMER(&lastProbeDuration);
		
		size_t index;
		size_t probeCount = CalculateProbeCountThisFrame();
		for(size_t i = 0; i < probeCount; ++i)
		{
			index = (indexFromLastFrame + i) % probes.size();
			probes[index].GenerateRays(cmd, raycountPerProbe);
		}
		
		size_t maxSinglePassRays = rayTracer->GetMaxSinglePassRays();
		size_t start = 0;
		for(size_t i = 0; i < maxSinglePassRays; ++i)
		{
			// No need to ensure (start + maxSinglePassRays) < the vector's size bc that's done by vector_view constructor
			rayTracer->RaytaceIntoScene(std::vector_view<raycast_cmd_t>(cmd, start, start + maxSinglePassRays), rayResult);
			start += i;
		}
		
		ComputeProbesFromRayResult(index, probeCount);
		indexFromLastFrame = index;
	}
};

class Renderer
{
	RayTracingSystem rayTracer;
	ProbeRenderingSystem diffuseGI;

	std::vector<MeshRenderer*> GetMeshes();

	void RenderUniverse(Universe* universe)
	{
		
	}
};